extern string_equals

section .text
	

	find_word:  ;di - str ptr ; si - dict ptr
		test rsi, 0
		jz ._end
		mov r8, rsi
		mov r9, rdi

		._loop:
			add rsi, 16
			
			call string_equals

			test rax, rax
			jnz ._found
			mov rsi, qword [r8]
			test rsi, rsi
			jz ._end
			mov r8, rsi
			mov rdi, r9
			jmp ._loop
		._found:
			mov rax, r8
		ret
		._end:
			xor rax, rax
		ret

